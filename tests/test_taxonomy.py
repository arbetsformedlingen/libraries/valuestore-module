import unittest

from valuestore import taxonomy


class TaxonomyTests(unittest.TestCase):
    def test_occupation_sv(self):
        self.assertEqual(taxonomy.OCCUPATION_SV, 'yrkesroll')  # add assertion here


if __name__ == '__main__':
    unittest.main()
